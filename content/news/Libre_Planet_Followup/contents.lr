title: Libre Planet Followup
---
pub_date: 2011-03-20
---
summary:
I attended Libre Planet in Boston this weekend with a goal of talking
about the FreedomBox to anybody who would listen.  I gave a lightning
talk to let people know it exists and was deluged with interest
afterwards in the hallway.  As expected, the FSF crowd has a lot of
great ideas, not just about how to implement the FreedomBox, but about
how to organize a project of this scope.
---
body:

I attended Libre Planet in Boston this weekend with a goal of talking
about the FreedomBox to anybody who would listen.  I gave a lightning
talk to let people know it exists and was deluged with interest
afterwards in the hallway.  As expected, the FSF crowd has a lot of
great ideas, not just about how to implement the FreedomBox, but about
how to organize a project of this scope.

Scores of people expressed interest in further volunteering.  I hope
to see them in IRC and the email discussion soon.  Rob Savoy, in
particular, is a fascinating individual who could teach us all a thing
or two about development.

I made some headway in arranging for teleconference facilities for the
FreedomBox Foundation.  IRC is great, but we're going to need some
group voice calls at various points.  I've added to my todo list an
item to make sure we can record calls so we have logs where
appropriate.

I talked specifically to some old free software experts, the hard core
hackers who have track records of pulling off ambitious projects.  I
believe I convinced some that this project is a place to put their
energy and that we'll see them active soon.

I invited a woman to the project who has a long history of improving
the communities she's in.  She is over committed for the next several
months, but I've scheduled a note to follow up with her.

I talked to several people who worked on OLPC or OpenMoko, other large
projects with some commonalities with the FreedomBox.  I received some
interesting and frank views about what went right and what went wrong
in those efforts.  Some opinions are most worth passing on:

* First, OLPC did not test its interface with end users early enough. I
talked to multiple people who thought this should have been done
sooner.  FreedomBox should put the target end user into the design
process early.

* Second, OLPC had some incredibly ambitious requirements that spilled
over from one part of the project to another.  The requirement that
the interface be usable by non-literate users drove a lot of the
innovative design, but it made some tasks quite hard.

* Third, OpenMoko folks spent a lot of time making a distribution--
packaging and recompiling all those Debian packages for their
platform.  It used a lot of resources and the repos were never
complete.  FreedomBox should not be a Debian-based distribution so
much as a a Debian-based project that relies on a lot of
already-existing packages.

Meshing is hard.  Nobody I met knows anybody who is nailing mesh
networks.  I'm going to get all the mesh heads together soon for a
real conversation to see if we can work towards a recommendation on
the most promising avenue.

Michael Stone pointed me to <a
href="http://www.cs.umbc.edu/~finin/home/heilmeyerCatechism.html">Heilmeyer's
Catechism</a>.  Those are some good questions.

Big thanks to Matt Lee at FSF for throwing a great conference.  And to
Deb Nicholson for hosting me and towing me around town.  She has great
ideas about how to include more people in FreedomBox.
