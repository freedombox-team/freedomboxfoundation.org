title: Announcing New Network Manager: Felix Freeman
---
pub_date: 2019-09-09
---
summary:

The FreedomBox Foundation is pleased to announce that we have recruited a new Network Manager: Felix Freeman. Felix is a free software activist based in Chile, and he brings a lot of energy and enthusiasm to the FreedomBox community.

---
body:

The FreedomBox Foundation is pleased to announce that we have recruited a new Network Manager: Felix Freeman. Felix is a free software activist based in Chile, and he brings a lot of energy and enthusiasm to the FreedomBox community.

Launched in February 2019, the Network Managers program supports volunteers (called "Network Managers") who advocate for FreedomBox in their communities. What makes Network Managers different from other FreedomBox users is their leadership and relationship with the FreedomBox Foundation. Network Managers are pioneers: they bring FreedomBox to communities that need it and educate their colleagues about the importance of decentralized digital services. To support Network Managers, the FreedomBox Foundation offers free resources and services, like hardware, consulting, special attention to patch and feature requests, and even on-site visits.

## About Felix Freeman

<p align="center"> 
<img src="/images/Felix_Freeman.jpg">
</p>
### <center>Image: Felix Freeman</center>

Based in Chile, Felix Freeman is a Web Developer, GNU/Linux SysAdmin, Software Engineer, and activist who specializes in libre software. Felix has been active in the free software community for about 15 years. Starting in 2005, he began participating in the now-defunct GNUCHILE Foundation as an advocate, while also doing some software development. In recent years, he has helped to organize several free software-focused events, such as a Software Freedom Day celebration, the FLISoL (latinoamerican installfest), and numerous free software training sessions. Throughout his career, he has utilized free software to solve real problems. His professional experience includes administering GNU/Linux servers and distributed systems and developing a fair amount of free software-based web applications for a wide range of fields. Felix’s activism centers on the education of free software, free knowledge, and free culture, which includes technological empowerment, ethical hacker culture for novice programmers, and the development of technical skills for hardcore nerds. He is also an advocate of non-violence.

Felix originally became interested in FreedomBox because he was looking for a way to decentralize the internet. He runs a FreedomBox at home and advocates for FreedomBox through the various activist networks in which he participates. He uses FreedomBox as a live example of network infrastructure and a practical means to empower people to take control of their computing.

Connect with Felix: [Personal Website](https://hacktivista.com) | [Business Inquires](https://hackware.cl) | [CuatroLibertades Telegram Group](https://t.me/cuatrolibertadeschile)

## Looking Forward

Felix is the FreedomBox Foundation's first Network Manager to reside outside the United States. We are eager to support his work in Chile and hope to continue expanding our presence throughout the globe. To help jumpstart Felix's work, the FreedomBox Foundation has already donated [Pioneer Edition FreedomBox kits](https://freedomboxfoundation.org/buy/) to him, and we look forward to supporting his ideas in the coming months.

If you want to be the next Network Manager, please reach out to <mailto:info@freedomboxfoundation.org> with "Network Manager" in the subject line. You can learn more about the Network Managers program here: <https://freedomboxfoundation.org/partners/>
