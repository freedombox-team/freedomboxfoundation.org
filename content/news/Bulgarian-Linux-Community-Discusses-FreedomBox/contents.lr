title: Bulgarian Linux Community Discusses FreedomBox
---
pub_date: 2019-05-31
---
summary:

We were very proud to see that a presentation about FreedomBox was delivered at a meeting of the [Linux Users Group - Bulgaria](http://linux-bulgaria.org/) (LUG-BG) in May 2019. The presentation was delivered in Bulgarian by Tsvetan Usunov, the Founder and Owner of [Olimex](https://www.olimex.com/).

If you speak Bulgarian or know anyone who does, be sure to watch and/or share this video: [LUG-BG 2019 LT: FreedomBox (Цветан Узунов)](https://www.youtube.com/watch?v=SJkkT42dUKs)


---
body:

We were very proud to see that a presentation about FreedomBox was delivered at a meeting of the [Linux Users Group - Bulgaria](http://linux-bulgaria.org/) (LUG-BG) in May 2019. The presentation was delivered in Bulgarian by Tsvetan Usunov, the Founder and Owner of [Olimex](https://www.olimex.com/).

If you speak Bulgarian or know anyone who does, be sure to watch and/or share this video: [LUG-BG 2019 LT: FreedomBox (Цветан Узунов)](https://www.youtube.com/watch?v=SJkkT42dUKs)

## Free Software in Bulgaria

The Linux Users Group – Bulgaria (LUG-BG) was established in 1997, and it hosts events for Bulgarian Linux users. In order to promote Bulgarian involvement in the global Linux community, the user group speaks and writes in Bulgarian in their events and operations, even encouraging the use of the Cyrillic script in their written material. Additionally, LUG-BG also promotes the translation of free and open source software into Bulgarian.

The FreedomBox Foundation is thrilled that our software was discussed by a community with such an admirable mission. Our goal has always been for FreedomBox to empower people throughout the world to reclaim the internet. But the goal of self-empowerment cannot be achieved without inclusive, local leadership. We therefore encourage user communities like LUG-BG to create spaces where locals can engage with our software in their native tongue.

We were particularly proud to see that the presentation was delivered by Tsvetan Usunov, Founder and Owner of Olimex. Olimex is a company that produces [open source hardware](https://en.wikipedia.org/wiki/Open-source_hardware) and, as of [April 2019](https://freedomboxfoundation.org/news/launching_sales/), the [Pioneer Edition FreedomBox](https://freedombox.org/buy/). Throughout the globe, Olimex is widely respected by the free and open source software community for its affordable products and support of Linux-based software. In Bulgaria, Olimex is a leader in the Linux user community.

## FreedomBox Localization in Bulgarian

After watching this presentation, we noticed that the FreedomBox software interface hadn't yet been translated into Bulgarian. From its birth, the FreedomBox community has always been committed to localization. [Currently, our community translates FreedomBox into more than 20 languages, and 8 languages have been translated in full or almost in full (85% or more of the strings)](https://hosted.weblate.org/projects/freedombox/plinth/). In 2017, we became an official partner of [Localization Lab](https://www.localizationlab.org/blog/2017/9/11/new-project-freedombox), with the specific aim of localizing FreedomBox in more places in the world.

Today, we created a new category on [our Weblate translation page](https://hosted.weblate.org/projects/freedombox/) for Bulgarian. We encourage anyone with proficiency in Bulgarian to help us translate our user interface. Simply click on the links below, register a free account, and start translating strings of text!

**Translate the FreedomBox user interface into Bulgarian**: <https://hosted.weblate.org/projects/freedombox/plinth/bg/> 

**Translate the FreedomBox Android app into Bulgarian**: <https://hosted.weblate.org/projects/freedombox/android/bg/>
